/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsclassification.java;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author ZANDUT
 */
public class ReadExcel
{

    private String inputFile;

    public ReadExcel(String inputFile)
    {
        this.inputFile = inputFile;
    }

    public ReadExcel()
    {
    }
    
    

    public void setInputFile(String inputFile)
    {
        this.inputFile = inputFile;
    }

    public ArrayList<Berita> read() throws IOException
    {
        File inputWorkbook = new File(inputFile);
        ArrayList<Berita> arrayBerita = new ArrayList<>();
        Workbook w;
        try
        {
            w = Workbook.getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);
            // Loop over first 10 column and lines

            for (int j = 0; j < sheet.getRows(); j++)
            {

                
                if (!sheet.getCell(1, j).getContents().equals(""))
                {
                    //ambil ID
                    Berita berita = new Berita(Integer.parseInt(sheet.getCell(0, j).getContents()));
    //                Cell isi_berita = sheet.getCell(1, j);
    //                System.out.println(isi_berita.getContents());

                    //ambil isi berita
                    Cell isi_berita = sheet.getCell(1, j);
                    String[] words = isi_berita.getContents().split(" ");
                    for (int i = 0; i < words.length; i++)
                    {

                        berita.addWord(words[i]);
                    }

                    //ambil labels
                    Cell labels = sheet.getCell(2, j);
                    words = labels.getContents().split(",");
                    for (int i = 0; i < words.length; i++)
                    {

                        Integer label = Integer.parseInt(words[i]);
                        berita.addLabel(label);
                    }

                    arrayBerita.add(berita);
                }

            }
        } catch (BiffException e)
        {
            e.printStackTrace();
        }

        return arrayBerita;
    }

    public void WriteExcel(File output, ArrayList<Berita> arrayBerita, ArrayList<String> vocab)
    {
        WritableWorkbook w2 = null;
        try
        {
            w2 = Workbook.createWorkbook(output);
            WritableSheet s2 = w2.createSheet("Sheet1", 0);
            
           

            for (int i = 0; i < arrayBerita.size(); i++)
            {
                
               
                
                ArrayList<Double> beritaFeature = arrayBerita.get(i).getFeatures();
                for (int j = 0; j < beritaFeature.size(); j++)
                {
                    Label labelFeatures = new Label(i, j, beritaFeature.get(j).toString());
                    s2.addCell(labelFeatures);
                }
//                
                for (int j = 0; j < arrayBerita.get(i).getLabels().size(); j++)
                {
                    Label labelLabel = null;
                    try
                    {
                        labelLabel = new Label(i, j+beritaFeature.size(), arrayBerita.get(i).getLabels().get(j).toString());
                    } catch (Exception e)
                    {
                        labelLabel = new Label(i, j+beritaFeature.size(), "0");
                    }
                    
                    s2.addCell(labelLabel);
                }
                   
                
            }
            w2.write();
            w2.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println("FINISHED");
    }


}
