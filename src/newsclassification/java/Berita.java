/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsclassification.java;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author ZANDUT
 */
public class Berita
{
    private Integer id;
    private ArrayList<String> words;
    private ArrayList<String> uniqueWord;
    private ArrayList<Integer> labels;
    private ArrayList<Double> features;

    public Berita(Integer id)
    {
        this.id = id;
        words = new ArrayList<>();
        uniqueWord = new ArrayList<>();
        labels = new ArrayList<>();
        features = new ArrayList<>();
    }
    
    public void addWord(String word)
    {
        words.add(word);
    }

    public ArrayList<String> getWords()
    {
        return words;
    }
    
    

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }

    public ArrayList<String> getUniqueWord()
    {
        return uniqueWord;
    }

    

    public ArrayList<Double> getFeatures()
    {
        return features;
    }

    public ArrayList<Integer> getLabels()
    {
        return labels;
    }

    

    
    
    public void addUniqueWord(String word)
    {
        uniqueWord.add(word);
    }
    
    public void addLabel(Integer label)
    {
        labels.add(label);
    }
    
    public void addFeature(Double value)
    {
        features.add(value);
    }

    public void setLabels(ArrayList<Integer> labels)
    {
        this.labels = labels;
    }
    
    
    
    
    
}
