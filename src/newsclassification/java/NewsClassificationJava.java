/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsclassification.java;

import TFIDF.IDF;
import TFIDF.TF;
import TFIDF.TF_IDF;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import preprocessing.CaseFolding;
import preprocessing.Lemmatization;

import preprocessing.StopWordRemoval;
import preprocessing.NonAlphaNumericRemoval;
import preprocessing.Tokenization;

/**
 *
 * @author ZANDUT
 */
public class NewsClassificationJava
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        ReadExcel file = new ReadExcel("./Dataset/Dataset_Klasifikasi_Multi-Label_Topik_Berita.xls");
        ArrayList<Set<String>> arrayALLDocument = new ArrayList<>();
        Set<String> vocabulary = new HashSet<>();
        ArrayList<Berita> arrayBerita = null;
        try
        {
            arrayBerita = file.read();
            for (int i = 0; i < arrayBerita.size(); i++)
            {
                
                Berita berita = arrayBerita.get(i);
                //getDataSet
                ArrayList<String> kata = berita.getWords();
                //Case Folding
                ArrayList<String> kataCaseFolding = new CaseFolding().toLowerCase(kata);
                //Tokenization
                ArrayList<String> kataToken = new Tokenization().Tokeniz(kataCaseFolding);
                // Tanda Baca Removal
                List<String> kataTandaBaca = new NonAlphaNumericRemoval().tandaBacaRemoval(kataToken);
                
                ArrayList<String> kataLemming = new Lemmatization().lemming(kataTandaBaca);
                ArrayList<String> kataStopWord = new StopWordRemoval().wordRemoval(kataLemming);
                //StopWord Removal

                for (int k = 0; k < kataStopWord.size(); k++)
                {
                    if (!kataStopWord.get(k).equals(""))
                    {
                        
                        berita.addUniqueWord(kataStopWord.get(k));
//                        if (!kataStopWord.get(k).equals("coba"))
                        vocabulary.add(kataStopWord.get(k));
                        
                    }
                }
                
                arrayBerita.set(i, berita);
            }
        } catch (IOException ex)
        {
            Logger.getLogger(NewsClassificationJava.class.getName()).log(Level.SEVERE, null, ex);
        }

//        
        ArrayList<String> vocabularies = new ArrayList<>(vocabulary);
        ArrayList<Double> idfs = new TF_IDF().getIDF(arrayBerita, vocabularies);
       int sum = 0;
        int indeks = 6;
        for (int i = 0; i < vocabularies.size(); i++)
        {
            int jumlah = 0;
            
            for (int j = 0; j < arrayBerita.get(indeks).getUniqueWord().size(); j++)
            {
                
                if (vocabularies.get(i).equals(arrayBerita.get(indeks).getUniqueWord().get(j)))
                {
                    jumlah ++;
                    
                }
                
                
                
            }
            sum += jumlah;
            System.out.println(vocabularies.get(i)+"("+jumlah+"), ");
            System.out.println(sum+" = "+arrayBerita.get(indeks).getUniqueWord().size());
        }
        
        
        
        for (int i = 0; i < arrayBerita.size(); i++)
        {
            Berita berita = arrayBerita.get(i);
            ArrayList<Double> tfs = new TF_IDF().getTF(arrayBerita.get(i), vocabularies);
            for (int j = 0; j < idfs.size(); j++)
            {
                berita.addFeature(tfs.get((j))*idfs.get(j));
                
            }
            
            ArrayList<Integer> label = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
            for (int j = 0; j < arrayBerita.get(i).getLabels().size(); j++)
            {
                label.set(arrayBerita.get(i).getLabels().get(j) -1, 1);
            }
            berita.setLabels(label);
            arrayBerita.set(i, berita);
            
            
        }
        
//        for (int i = 0; i < arrayBerita.get(indeks).getFeatures().size(); i++)
//        {
//            System.out.println(arrayBerita.get(indeks).getFeatures().get(i));
//        }
        
        new ReadExcel().WriteExcel(new File("./Dataset/ann/vectorInput.xls"), arrayBerita, vocabularies);
        
//        for (int i = 0; i < arrayBerita.size(); i++)
//        {
//            System.out.println("---------------------------------------------------------");
//            System.out.println("Berita " + i);
//            for (int j = 0; j < arrayBerita.get(i).getFeatures().size(); j++)
//            {
//                System.out.println(arrayBerita.get(i).getFeatures().get(j));
//            }
//            System.out.println("DIMENSI : "+arrayBerita.get(i).getFeatures().size());
//            System.out.println("---------------------------------------------------------");
//        }
    }
    
}
