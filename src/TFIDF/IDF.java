/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TFIDF;

import newsclassification.java.Berita;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZANDUTg
 */
public class IDF
{

    public Double getValue(ArrayList<Berita> documents, String term)
    {
        double n = 0;
        for (int i = 0; i < documents.size(); i++)
        {
            Berita berita = documents.get(i);
            for (int k = 0; k < berita.getUniqueWord().size(); k++)
            {

                if (berita.getUniqueWord().get(k).equalsIgnoreCase(term))
                {
                    n++;
                    break;
                }

            }

        }

        return Math.log((documents.size() / n));
    }
}
