/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TFIDF;


import java.util.ArrayList;
import newsclassification.java.Berita;

/**
 *
 * @author ZANDUT
 */
public class TF_IDF
{

    public ArrayList<Double> getTF(Berita documents, ArrayList<String> vocab)
    {
        ArrayList<Double> arrayValue = new ArrayList<>();
        ArrayList<Berita> arrayBerita = new ArrayList<>();
        int jml = 0;
        for (int i = 0; i < vocab.size(); i++)
        {
            
            Double tf = new TF().getValue(documents.getUniqueWord(), vocab.get(i));
            
            arrayValue.add(tf);

        }

        return arrayValue;

    }

    public ArrayList<Double> getIDF(ArrayList<Berita> documents, ArrayList<String> vocab)
    {
        ArrayList<Double> arrayValue = new ArrayList<>();
        ArrayList<Berita> arrayBerita = new ArrayList<>();
        int jml = 0;
        for (int i = 0; i < vocab.size(); i++)
        {

            Double idf = new IDF().getValue(documents, vocab.get(i));

            arrayValue.add(idf);

        }

        return arrayValue;
    }
}
