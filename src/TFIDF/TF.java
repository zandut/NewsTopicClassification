/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TFIDF;


import java.util.ArrayList;

/**
 *
 * @author ZANDUT
 */
public class TF
{

    public Double getValue(ArrayList<String> data, String term)
    {
        double jumlah = 0;
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).equalsIgnoreCase(term))
            {
                jumlah++;
            }
        }
        
       return Math.log(jumlah + 1);
    }
}
