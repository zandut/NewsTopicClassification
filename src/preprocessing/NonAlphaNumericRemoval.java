/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preprocessing;

import com.gyosh.worker.utility.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

/**
 *
 * @author ZANDUT
 */
public class NonAlphaNumericRemoval
{
    public List<String> tandaBacaRemoval (ArrayList<String> array)
    {
        
        String sentence = Util.join(array, " ");
        String cleanedSentence = sentence.replaceAll("[^0-9A-Za-z]|[0-9]+", " ").trim();
        return Arrays.asList(cleanedSentence.split("\\s+"));
    }
}
