/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preprocessing;

import java.util.ArrayList;

/**
 *
 * @author ZANDUT
 */
public class Tokenization
{
    public ArrayList<String> Tokeniz (ArrayList<String> array)
    {
        ArrayList<String[]> arrayBaru = new ArrayList<>();
        for (String kata : array)
        {
            String[] splitan = kata.split(" ");
            arrayBaru.add(splitan);
        }
        ArrayList<String> arrayString = new ArrayList<>();
        for (String[] k : arrayBaru)
        {
            for (int i = 0; i < k.length; i++)
            {
                arrayString.add(k[i]);
            }
        }
        return arrayString;
    }
}
