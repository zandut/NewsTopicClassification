/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preprocessing;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ZANDUT
 */
public class StopWordRemoval
{
    public ArrayList<String> wordRemoval (List<String> array)
    {
        DataInputStream dis = null;
         ArrayList<String> arrayKamus = new ArrayList<>();
        try
        {
           
            dis = new DataInputStream(new FileInputStream(new File("./Kamus/stopword_list.txt")));
            String kata;
            while ((kata = dis.readLine()) != null)
            {
                
                arrayKamus.add(kata);
                    
            }
            
        } catch (Exception ex)
        {
            Logger.getLogger(StopWordRemoval.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        ArrayList<String> arrayBaru = new ArrayList<>();
        boolean ketemu = false;
        for (int i = 0; i < array.size(); i++)
        {
            
            for (int j = 0; j < arrayKamus.size(); j++)
            {
                if (!array.get(i).equals(arrayKamus.get(j)))
                {
                    
                    ketemu = false;
                }
                else
                {
                    
                    ketemu = true;
                    break;
                }
            }
            
            if (!ketemu)
            {
                arrayBaru.add(array.get(i));
            }
            else
            {
                ketemu = false;
            }
        }
        
        return arrayBaru;
        
    }
}
