/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preprocessing;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsastrawi.morphology.DefaultLemmatizer;
import jsastrawi.morphology.Lemmatizer;

/**
 *
 * @author ZANDUT
 */
public class Lemmatization
{

    public ArrayList<String> lemming(List<String> array)
    {

        Set<String> dictionary = new HashSet<String>();

        InputStream in = Lemmatizer.class.getResourceAsStream("/root-words.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        try
        {
            while ((line = br.readLine()) != null)
            {
                dictionary.add(line);
            }
        } catch (IOException ex)
        {
            Logger.getLogger(Lemmatization.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<String> arrayBaru = new ArrayList<>();
        Lemmatizer lemmatize = new DefaultLemmatizer(dictionary);
        for (String kata : array)
        {
            arrayBaru.add(lemmatize.lemmatize(kata));
        }

        return arrayBaru;
    }
}
