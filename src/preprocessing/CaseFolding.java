/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preprocessing;

import java.util.ArrayList;

/**
 *
 * @author ZANDUT
 */
public class CaseFolding
{
    public ArrayList<String> toLowerCase(ArrayList<String> array)
    {
        ArrayList<String> arrayBaru = new ArrayList<>();
        for (String kata : array)
        {
            arrayBaru.add(kata.toLowerCase());
        }
        
        return arrayBaru;
    }
    
}
