function [w1 w2 biasInput biasOutput]=backPro(A1,A2,E,w1,w2,learningrate,inputNeuron,biasInput,biasOutput)
    D2 = A2.*(1-A2).*E;
    D1 = A1.*(1-A1).* (D2'*w2')';
    dw2 = (A1 * D2') * learningrate;
    dw1 = (inputNeuron'*D1') * learningrate;
    db1 = learningrate * D1;
    db2 = learningrate * D2;
    w1 = w1 + dw1;
    w2 = w2 + dw2 ;
    biasInput = biasInput+db1;
    biasOutput = biasOutput+db2;
end