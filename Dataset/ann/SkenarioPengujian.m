load matlab.mat;
berhenti = 0;
makPercobaan =50 ;
i = 1;
clear sound;
loss = [];
epoch = [200,200,200,200,200,200,200,200,400, 400,400,400,400,400,400,400];
learning_rate = [0.008,0.008,0.008,0.008,0.02,0.02,0.02,0.02,0.008,0.008,0.008,0.008,0.02,0.02,0.02,0.02];
neuron = [10,10,20,20,10,10,20,20,10,10,20,20,10,10,20,20];
alpha = [0.1,0.3,0.1,0.3,0.1,0.3,0.1,0.3,0.1,0.3,0.1,0.3,0.1,0.3,0.1,0.3];

for (a=16:16)
    
    while (i <= makPercobaan)
        fprintf('Parameter ke : %d \n',a);
        [predict A2 A3 MSE w1 w2 b1 b2 HammingLoss, flag] = annMLP(data_latih, data_uji,target_latih, target_uji,neuron(a),13,epoch(a),learning_rate(a), alpha(a));
        loss(i, :) = HammingLoss(1, :);
        fprintf('Percobaan ke : %d \n',i);


        i = i + 1;

    end
    
    kecil(a) = min(loss(:));
    sound(y, Fs);
    a = a+1;
    i = 1;
    
    save('matlab');
    
end

plot(kecil);
sound(y, Fs);
