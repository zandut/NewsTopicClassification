function [predictTarget A2 A3 MSE w1 w2 biasInput biasOutput HammingLoss, flag] = annMLP(data_latih, data_uji,T_latih, T_uji,jumlahHiddenNeuron,jumlahOutputNeuron,epoch,learningrate, alpha)
    predictTarget = [];
    MaxMSE = 10^-4;
    A2 = [];
    A3 = [];
    HammingLoss = [];
    flag = 0;
    sebelum = 0;
    jumlahInput = length(data_latih(1, :));
    
    %pembangkitan weight antara input layer dan hidden layer
    w1=[];    
    a=-0.99;
    b=0.99;
    for(i=1:jumlahHiddenNeuron)
        w1=[w1; ((b-a).*rand(1,length(data_latih(1,1:jumlahInput)))+a)];
    end
    w1=w1';
    
    %pembangkitan weight antara hidden layer dan output layer
    w2=[];
    for(i=1:jumlahOutputNeuron)
        w2=[w2; ((b-a).*rand(1,jumlahHiddenNeuron)+a)];
    end
    w2 = w2';
    
    %pembangkitan biasinput dan biasoutput
    biasInput = 1-rand(1,jumlahHiddenNeuron);
    biasOutput = 1-rand(1,jumlahOutputNeuron);
    
    biasInput = biasInput';
    biasOutput = biasOutput';
    
    MSE = [];
    
%     fprintf('Length Input %d ',length(M3(:, 1)));
    
    eph=1;
    berhenti = 0;
    %train
    Error=1
    while ((epoch>=eph) && (Error > MaxMSE))
        MSEepoch = 0;
        for(i=1:length(data_latih(:, 1)))
            cp = data_latih(i,1:jumlahInput);
            ct = T_latih(i,:);
            
            %feedforward
            [A1 A2]=feedForward(cp,length(cp),jumlahHiddenNeuron,w1,w2,jumlahOutputNeuron,biasInput,biasOutput, alpha);    
            E = ct' - A2;
            sumE=0.0;
            for(i=1:length(E))
                sumE=sumE+(E(i)^2);
            end
            EJaringan = sqrt(sumE);
            MSEepoch = MSEepoch + EJaringan^2;
            %backpro
            [w1 w2 biasInput biasOutput]=backPro(A1,A2,E,w1,w2,learningrate,cp,biasInput,biasOutput); 
        end;
        Error = (MSEepoch)/length(data_latih);            
        fprintf('MSE ke-%d : %8.10f \n',eph,Error);
        MSE=[MSE; Error];
       
        
        
        for(i=1:length(data_uji(:, 1)))
            cp = data_uji(i,1:jumlahInput);
            ct = T_uji(i,:);
            [A1 A2]=feedForward(cp,length(cp),jumlahHiddenNeuron,w1,w2,jumlahOutputNeuron,biasInput,biasOutput, alpha);    
            A3(:, i) = A2;
        end
            
            
            row = length(T_uji(1, :));
                column = length(T_uji(:, 1));
                takSama = 0;
            for (i=1:column)
                for (j=1:row)
                    if (round(A3(j, i)) ~= T_uji(i, j))
                        takSama = takSama + 1;
                    end
                end
            end
            HammingLoss(1, eph) = (1 / (row * column)) * takSama;
            

            fprintf('Hamming Loss ke-%d : %8.10f \n',eph,HammingLoss(1, eph));
%             if (eph > 1)
%                 if (HammingLoss(1, eph - 1) < HammingLoss(1, eph))
% 
%                    berhenti = 1;
%                    flag = 1;
%                 end
%             end
            eph = eph+1;
            
%             if (eph > epoch && berhenti == 1)
%                break; 
%             end
                
        end
               
     
%     plot(HammingLoss);
%     xlabel('Epoch');
%     ylabel('HammingLoss');
end