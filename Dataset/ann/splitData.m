function [data_latih, data_uji, target_latih, target_uji] = splitData(input, target)
%    k=5; jumlah data / k;
    k = 5;
    input = input';
    target = target';
    data_latih = [];
    data_uji = [];
    sizeData = length(input(:, 1));
    porsi = round(sizeData / k);
    indeksTesting = randperm(porsi);
    indeksTraining = randperm(sizeData-porsi);
    for (i=1:length(indeksTraining))
       if (i < length(porsi) + 1)
          indeksTraining(indeksTesting(i)) = 0; 
       end
        
    end
    for (i=1:length(indeksTraining))
       
        if (indeksTraining(i) ~= 0)
            data_latih(i, :) = input(indeksTraining(i), :);
            target_latih(i, :) = target(indeksTraining(i), :);
        end
       
    end
     for(i=1:length(indeksTesting))
         data_uji(i, :) = input(indeksTesting(i), :);
         target_uji(i, :) = target(indeksTesting(i), :);
     end
end