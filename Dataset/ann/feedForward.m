function [A1 A2 A3]=feedForward(inputNeuron,jumlahInputNeuron,jumlahHiddenNeuron,weightInput,weightOutput,jumlahOutputNeuron,biasInput,biasOutput, alpha)
    A1=[];
    A2=[];
    %input ke hidden A1 = (P * W1)+B1
    for(i=1:jumlahHiddenNeuron)
        sum=0.0;
        for(j=1:jumlahInputNeuron)
            sum = sum + (inputNeuron(j)*weightInput(j,i));
        end
        sum = sum + biasInput(i);
        A1=[A1; (1/(1+exp(-alpha*sum)))];
    end
    
    %hidden ke output
    for(i=1:jumlahOutputNeuron)
        sum = 0.0;
        for(j=1:jumlahHiddenNeuron)
            sum = sum + (A1(j)*weightOutput(j,i));
        end
        sum = sum + biasOutput(i);
        A2 = [A2; (1/(1+exp(-alpha*sum)))];
    end
    
end